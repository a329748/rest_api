const express = require('express');
const app = express();
const PORT = 8080;

const router = express.Router({ mergeParams: true });

app.use(express.json())

app.listen(
	PORT,
	() => console.log(`The API is live @ localhost:${PORT}`)
)

app.get('/results/:n1/:n2', (req, res) => {
	const{ n1 } = req.params;
	const{ n2 } = req.params;

	if(!n1 || !n2) {
		res.status(418).send({message:"ERROR: Missing parameters."})
	}

	res.status(200).send({
		n1: n1,
		n2: n2,
		result: parseFloat(n1) + parseFloat(n2)
	})
});

app.post('/results/:n1/:n2', (req, res) => {
	const{ n1 } = req.params;
	const{ n2 } = req.params;

	if(!n1 || !n2) {
		res.status(418).send({message:"ERROR: Missing parameters."})
	}

	res.status(200).send({
		n1: n1,
		n2: n2,
		result: parseFloat(n1) * parseFloat(n2)
	})
});

app.put('/results/:n1/:n2', (req, res) => {
	const{ n1 } = req.params;
	const{ n2 } = req.params;

	if(!n1 || !n2) {
		res.status(418).send({message:"ERROR: Missing parameters."})
	}

	res.status(200).send({
		n1: n1,
		n2: n2,
		result: parseFloat(n1) / parseFloat(n2)
	})
});

app.patch('/results/:n1/:n2', (req, res) => {
	const{ n1 } = req.params;
	const{ n2 } = req.params;

	if(!n1 || !n2) {
		res.status(418).send({message:"ERROR: Missing parameters."})
	}

	res.status(200).send({
		n1: n1,
		n2: n2,
		result: Math.pow(parseFloat(n1), parseFloat(n2))
	})
});

app.delete('/results/:n1/:n2', (req, res) => {
	const{ n1 } = req.params;
	const{ n2 } = req.params;

	if(!n1 || !n2) {
		res.status(418).send({message:"ERROR: Missing parameters."})
	}

	res.status(200).send({
		n1: n1,
		n2: n2,
		result: parseFloat(n1) - parseFloat(n2)
	})
});